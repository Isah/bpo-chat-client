package com.uzaal.bpochat.signup

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.uzaal.bpochat.R
import kotlinx.android.synthetic.main.fragment_welcome.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [WelcomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class WelcomeFragment : Fragment() {
    // TODO: Rename and change types of parameters

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater.inflate(R.layout.fragment_welcome, container, false)



        v.welcome_button.setOnClickListener {

            onButtonClicked()

        }


        return v

    }


    fun onButtonClicked(){

        var frag  = SignUpFragment()

        val hfragmentManager: FragmentManager = activity!!.supportFragmentManager
        hfragmentManager.beginTransaction()
            .replace(R.id.signup_fragment, frag)
            .commit()
    }



    companion object {

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            WelcomeFragment().apply {

            }
    }


}
