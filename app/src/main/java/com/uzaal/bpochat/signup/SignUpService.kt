package com.uzaal.bpochat.signup

import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.annotation.RestrictTo
import com.example.controllers.SignUpController
import com.example.usecases.signup.ConnectionConfig
import com.example.usecases.utilities.AppExecutors
import com.uzaal.bpochat.injection.MyInjection
import java.util.*


class SignUpService : Service() {



    private var userName: String? = null
    private var passWord: String? = null
    private val message: String? = null
    private val from: String? = null
    private val to: String? = null
    private var HOST: String? = null
    private var HOST_ADDRESS: String? = null


    private lateinit var controller:SignUpController



    // private ChatXmpp myXMPP = ChatXmpp.getInstance();
    private val appExecutors: AppExecutors = AppExecutors()
    @RestrictTo(RestrictTo.Scope.TESTS)
    var started = false
    @RestrictTo(RestrictTo.Scope.TESTS)
    var stopped = false
    @RestrictTo(RestrictTo.Scope.TESTS)
    var isOnStartCommandStarted = false
    //1. Binder given to clients to bind to services
    private val mBinder: IBinder = LocalBinder()

    /**2
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods

        fun getService(): SignUpService { // Return this instance of LocalService so clients can call public methods
            return this@SignUpService
        }
    }


    // To provide binding for a service i need to implement onBind()
// returns a IBinder that provides a programming interface that clients
// can use to interact with the service
    override fun onBind(intent: Intent): IBinder {

            passWord = intent.getStringExtra("pwd")
            userName = intent.getStringExtra("usr")
            HOST = intent.getStringExtra("host")
            HOST_ADDRESS = intent.getStringExtra("host_address")
            Log.d("SERVICE CONN BOUND: ", "time: " + Date())

        return mBinder
    }


    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
       //

        passWord = intent?.getStringExtra("pwd")
        userName = intent?.getStringExtra("usr")
        HOST = intent?.getStringExtra("host")
        HOST_ADDRESS = intent?.getStringExtra("host_address")




        val handler = Handler()
        val runnable = Runnable {
            ///   Log.d("SERVICE CONN EST: ", "time: " + Date())
            //   notifyConnectionEstablished()

            registerEjabberdAccount(userName!!,passWord!!)


        }
        handler.postDelayed(runnable, 5000)
        appExecutors.diskIO().execute(runnable)


        return super.onStartCommand(intent, flags, startId)
    }


    override fun onCreate() {
        super.onCreate()
        started = true
        //service object is initiated

    }

    override fun onDestroy() { // myXMPP.disconnectConnection();
        super.onDestroy()
        stopped = true
    }








    private var mIsConnectionEstablished = false

    interface OnConnectionEstablishedListener {
        fun onConnectionEstablished()
    }

    private var mListener: OnConnectionEstablishedListener? = null
    //the one that impl this interface will get the call back
    fun setOnConnectionEstablishedListener(listener: OnConnectionEstablishedListener?) {
        mListener = listener
        // Already connected to server. Notify immediately.
        if (mIsConnectionEstablished) {
            mListener!!.onConnectionEstablished()
        }
    }

    private fun notifyConnectionEstablished() {
        mIsConnectionEstablished = true
        if (mListener != null) {
            mListener!!.onConnectionEstablished()
        }
    }


    var myXMPPConnection: ConnectionConfig? =
        ConnectionConfig.instance



    //Acts 3:19


    //My long running task
    private fun registerEjabberdAccount(user: String, pass: String) { //throws XMPPException, SmackException.NoResponseException, SmackException.NotConnectedException

      //  controller = MyInjection(cont).getSignUpController()

        //then call the controller to deal withis
        //s.createEjAccount(user,pass)

    }
}