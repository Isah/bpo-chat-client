package com.uzaal.bpochat.signup

import android.content.Context
import android.content.SharedPreferences

class SessionManager(var _context: Context) {
    // Shared Preferences
    var pref: SharedPreferences
    var editor: SharedPreferences.Editor
    // Shared pref mode
    var PRIVATE_MODE = 0

    fun setLogin(isLoggedIn: Boolean) {
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn)
        // commit changes
        editor.commit()
    }

    fun setSignup(isSignup: Boolean) {
        editor.putBoolean(KEY_IS_SIGN_UP, isSignup)
        // commit changes
        editor.commit()
    }



    fun isLoggedIn(): Boolean {
        return pref.getBoolean(SessionManager.KEY_IS_LOGGEDIN, false)
    }

    fun isSignedUp(): Boolean {
        return pref.getBoolean(SessionManager.KEY_IS_SIGN_UP, false)
    }

    companion object {
        // LogCat tag
        private val TAG = SessionManager::class.java.simpleName
        // Shared preferences file name
        private const val PREF_NAME = "AndroidHiveLogin"
        private const val KEY_IS_LOGGEDIN = "isLoggedIn"

        private const val KEY_IS_SIGN_UP = "AndroidSignUp"
    }

    init {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }
}