package com.uzaal.bpochat.signup

import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.controllers.SignUpController
import com.example.presenters.SignUpView
import com.example.presenters.SignUpViewModel
import com.example.usecases.config.HostConfig
import com.example.usecases.utilities.PreferenceHandler
import com.uzaal.bpochat.R
import com.uzaal.bpochat.chat.ChatsHistoryActivity
import com.uzaal.bpochat.injection.MyInjection
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.android.synthetic.main.fragment_sign_up.view.*

/**
 * A simple [Fragment] subclass.
 */
class SignUpFragment : Fragment(),SignUpView {

  //  @Inject lateinit var controller : SignUpController

    private val m_serviceBound = false


    var pDialog : ProgressDialog ? = null

    var controller : SignUpController ? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //MyApplication.graph.inject(this)
        pDialog = ProgressDialog(context)

       controller = MyInjection(this).getSignUpController()

        // Check if user is already logged in or not


    }


    lateinit var session : SessionManager


  //  var rootView : View ? =null

     var user : String ? = null
     var pass : String ? = null
     var phone : String ? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

     var rootView = inflater.inflate(R.layout.fragment_sign_up, container, false)

        rootView.signup_button.setOnClickListener {

                pass = rootView.password.text.toString().trim { it <= ' ' }
                user = rootView.username.text.toString().trim { it <= ' ' }
                phone = rootView.phone.text.toString().trim {it <= ' '}


                controller!!.register(user!!,pass!!, phone!!)

        }


        rootView.activate_button.setOnClickListener {

            val code = rootView.activation_code.text.toString().trim{it <= ' '}
            controller!!.activateUser(user!!,pass!!,phone!!,code)


        }



        return rootView
    }



    override fun showInvalidInput(signUpViewModel: SignUpViewModel) {
        Toast.makeText(context,signUpViewModel.invalidMsg!!,Toast.LENGTH_LONG).show()

    }

    override fun showProgress(signUpViewModel: SignUpViewModel) {
        //Progress
        if(signUpViewModel.isLoadProgess){ showDialog() }else{ hideDialog()}

    }

    override fun showSignUpView(signUpViewModel: SignUpViewModel) {


            //Verification
            if (!signUpViewModel.isSigned){

                layout_verify.visibility = View.VISIBLE

                layout_signup.visibility = View.INVISIBLE
                signup_button.visibility =  View.INVISIBLE

               // activation_title.text = signUpViewModel.verificationMsg
                activation_subtitle.text = signUpViewModel.verificationMsg

                activate_button.visibility =  View.VISIBLE
                activate_button.text = resources.getString(R.string.signup_button_activate)



            } else {

                val pref = PreferenceHandler(context!!)
                pref.putChatUserPref(signUpViewModel.user!!.userName,signUpViewModel.user!!)

                //textID.text = signUpViewModel.welcomeMsg
                session = SessionManager(context!!)
                session.setSignup(true)
                session.setLogin(true)

              //  Toast.makeText(context,signUpViewModel.welcomeMsg,Toast.LENGTH_LONG).show()

                //client makes request to create account
             //  createAccount(signUpViewModel.user?.userName,signUpViewModel.user?.password)

                openChatsHistory(signUpViewModel.welcomeMsg!!)



            }


    }





    private fun openChatsHistory(msg:String){

        if (session.isLoggedIn()) { // User is already logged in. Take him to main activity


            val b  = Bundle()
            b.putString("welcome",msg)

            val intent = Intent(activity, ChatsHistoryActivity::class.java)
            intent.putExtras(b)
            startActivity(intent)
            activity?.finish()
        }
    }



    fun pickupPhone(){

        // Construct a request for phone numbers and show the picker

        /*
       val hintRequest = : HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build();

        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(apiClient, hintRequest);
        startIntentSenderForResult(intent.getIntentSender(),
            RESOLVE_HINT, null, 0, 0, 0);
           */

    }






    private fun showDialog() {
        if (!pDialog!!.isShowing) pDialog!!.show()
    }

    private fun hideDialog() {
        if (pDialog!!.isShowing) pDialog!!.dismiss()
    }








    private lateinit var binderService: SignUpService
    var mBound = false

    lateinit var serviceCon :ServiceConnection

    //1. the controller calls the usecase once registration and auth is complete
    //2. It calls the service to xmpp


    fun createAccount(userName: String?, pass: String?){


        // Bind to LocalService


        serviceCon = object : ServiceConnection{

            override fun onServiceConnected(name: ComponentName, service: IBinder) {
                Log.d("Bound Service Conn", "Inside")

                //why don't i just register the guy here ,but again how will i receive back the registration
                binderService = (service as SignUpService.LocalBinder).getService()
                Log.d("Bound Service Conn", "" + binderService.started)

                mBound = true


                if(mBound){

                    Log.d("Bound Service Conn", "IsBound:" + mBound)


                }



            }

            override fun onServiceDisconnected(name: ComponentName?) {


            }

        }



        val intent = Intent(activity, SignUpService::class.java)
        // User user = HostConfig.getUser(myContext);
        intent.putExtra("usr", userName)
        intent.putExtra("pwd", pass)
        intent.putExtra("host", HostConfig.HOST)
        intent.putExtra("host_address", HostConfig.HOST_ADDRESS)
        Log.d("Bound Service Before", "" + context!!.packageName)




        activity!!.startService(intent)
        //activity!!.bindService(myintent, serviceCon, Context.BIND_AUTO_CREATE)


    }



    override fun onDestroy() {
        super.onDestroy()

    //activity!!.unbindService(serviceCon)
    }














}
