package com.uzaal.bpochat.chat

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.controllers.ModuleControllers
import com.example.presenters.ChatView
import com.example.presenters.ChatViewModel
import com.example.usecases.chat.ModuleChats
import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser
import com.example.usecases.utilities.PreferenceHandler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.uzaal.bpochat.R
import com.uzaal.bpochat.injection.ModulePresenters
import java.util.*

class ChatViewActivity : AppCompatActivity(),ChatView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_view)


       val bundle  = intent.extras

       val jsonContact = bundle?.getString("contact","")

       val user = fromStringToUser(jsonContact)

        val toast = Toast.makeText(applicationContext,user?.userName,Toast.LENGTH_LONG)
        toast.show()

    }


    fun fromStringToUser(value: String?): ChatUser? {
        val listType =
            object : TypeToken<ChatUser?>() {}.type
        return Gson().fromJson(value, listType)
    }



    val moduleController = ModuleControllers()
    val modulePresenter  = ModulePresenters()


    fun executeSendChat(){

        val repo = ModuleChats().provideChatRepository(ModuleChats().provideChatDao(application)!!,ModuleChats().provideChatServiceClient(this),ModuleChats().provideReadChatServiceClient(this))

        val presenter = modulePresenter.provideChatsPresenter(this)
        val sendChats = ModuleChats().provideSendChatUseCase(repo)
        val getChats = ModuleChats().provideGetChatsUseCase(repo)

        moduleController.provideChatsController(presenter,sendChats,getChats)

        val send_controller =  moduleController.provideChatsController(presenter,sendChats,getChats)
        val pref = PreferenceHandler(this)
        val chatUser = pref.getChatUserPref(PreferenceHandler.USER_EMAIL)


        val bpoChat = BpoChat("","petr","0758591842","hello",1,2, Date(),"","")
        send_controller.executeChat(chatUser!!,bpoChat)

    }

    fun executeReadChats(){

        val repo = ModuleChats().provideChatRepository(ModuleChats().provideChatDao(application)!!,ModuleChats().provideChatServiceClient(this),ModuleChats().provideReadChatServiceClient(this))

        val presenter = modulePresenter.provideChatsPresenter(this)
        val sendChats = ModuleChats().provideSendChatUseCase(repo)
        val getChats = ModuleChats().provideGetChatsUseCase(repo)

         val controller =  moduleController.provideChatsController(presenter,sendChats,getChats)

         val pref = PreferenceHandler(this)
         val chatUser = pref.getChatUserPref(PreferenceHandler.USER_EMAIL)

         controller.executeReadIncomingChats(chatUser!!)

    }


    override fun showChats(chatsViewModel: ChatViewModel) {

    }

    override fun showHeader() {


    }


}
