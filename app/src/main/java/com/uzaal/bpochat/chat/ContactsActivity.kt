package com.uzaal.bpochat.chat

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.usecases.entities.ChatUser
import com.example.usecases.entities.ChatUserStatus
import com.google.gson.Gson
import com.uzaal.bpochat.R
import kotlinx.android.synthetic.main.activity_contacts.*

class ContactsActivity : AppCompatActivity(),ContactsActivityAdapter.Contactslistner {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts)


        setUpToolBar()
        setUpRecyclerView()
    }




    private fun setUpToolBar() {

        var toolbar = findViewById<Toolbar>(R.id.toolbar_contacts)

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val str = SpannableStringBuilder("Contacts")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str

        supportActionBar!!.title = str


    }


    private fun setUpRecyclerView(){

        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerContacts.layoutManager = layoutManager

        // decoration = DividerItemDecoration(getContext(), VERTICAL)
        //  val itemDecoration = ItemOffsetDecoration(getContext(), R.dimen.item_offset_schedule)

        //setting animation on scrolling
        recyclerContacts.setHasFixedSize(true); //enhance recycler view scroll
        recyclerContacts.isNestedScrollingEnabled = false;// enable smooth scrooll

        val adapter = ContactsActivityAdapter(this,getList(),this)
        adapter.setHasStableIds(true)
        adapter.notifyItemInserted(0)
        recyclerContacts.adapter  = adapter


    }

    private fun getList():List<ChatUser>{

       // val chatAdd = ChatContact("Add chat","","Add Group",R.drawable.ic_add_circle_black_24dp)
       // val chat1 = ChatContact("Peter","peter@localhost","Hi there",R.drawable.ic_person_black_24dp)



        val status = ChatUserStatus()
        status.profile = R.drawable.ic_add_circle_black_24dp
        status.statusMsg = "Hi there"

        val chatAdd = ChatUser("Add chat","","Add Group","",null)
        val chat1 = ChatUser("Peter","hello","+256 758591842","petr@loclahost",status)

        val list = ArrayList<ChatUser>()
        list.add(chatAdd)
        list.add(chat1)

        return list
    }



    override fun onContactClick(view: View, pos: Int, contact: ChatUser) {


        val gson = Gson()
        val json: String = gson.toJson(contact)

        val intent = Intent(this, ChatViewActivity::class.java)
        val bundle = Bundle()
        bundle.putString("contact",json)
        intent.putExtras(bundle)
        startActivity(intent)

    }


}
