package com.uzaal.bpochat.chat

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.usecases.entities.ChatHistory
import com.google.android.material.snackbar.Snackbar
import com.uzaal.bpochat.R
import kotlinx.android.synthetic.main.activity_chats_history.*


class ChatsHistoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chats_history)

        setUpToolBar()
        setUpRecyclerView()

        val wel = intent.extras?.getString("welcome")

        Log.d("RegMsg","$wel")


        val fab: View = findViewById(R.id.fab)
        fab.setOnClickListener { view ->

             /*
            Snackbar.make(view, "Here's a Snackbar", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show()

              */

          openContacts()




        }


        //chats_history_title.text = wel


    }

    private fun openContacts(){

        val intent = Intent(this, ContactsActivity::class.java)
        startActivity(intent)
       // finish()

    }

    private fun setUpToolBar() {

        var toolbar = findViewById<Toolbar>(R.id.toolbar_main)

        //    toolbar_main.logo = resources.getDrawable(R.drawable.ic_logo)
        // toolbar_main.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp)
        setSupportActionBar(toolbar)
        //supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        //  supportActionBar!!.setIcon(R.drawable.ic_logo)
        supportActionBar!!.setDisplayShowTitleEnabled(true)

        val str = SpannableStringBuilder("UZ Messenger")
        str.setSpan(StyleSpan(Typeface.BOLD), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        toolbar.title = str

        supportActionBar!!.title = str


    }


    private fun setUpRecyclerView(){

        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerChatHistory.layoutManager = layoutManager

        // decoration = DividerItemDecoration(getContext(), VERTICAL)
        //  val itemDecoration = ItemOffsetDecoration(getContext(), R.dimen.item_offset_schedule)

        //setting animation on scrolling
        recyclerChatHistory.setHasFixedSize(true); //enhance recycler view scroll
        recyclerChatHistory.isNestedScrollingEnabled = false;// enable smooth scrooll

        val adapter = ChatHistoryAdapter(this,getList())
        adapter.setHasStableIds(true)
        adapter.notifyItemInserted(0)
        recyclerChatHistory.adapter  = adapter


    }

    private fun getList():List<ChatHistory>{

        val chatAdd = ChatHistory(R.drawable.ic_add_circle_black_24dp,"Add chat","start a new chat or group")
        val chat1 = ChatHistory(R.drawable.ic_person_black_24dp,"Eden","How are you!")
        val chat2 = ChatHistory(R.drawable.ic_person_black_24dp,"Daniel","Hi iam fine!")

        val list = ArrayList<ChatHistory>()
        list.add(chatAdd)
        list.add(chat1)
        list.add(chat2)

        return list
    }





}
