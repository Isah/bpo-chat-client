package com.uzaal.bpochat.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.usecases.entities.ChatHistory
import com.uzaal.bpochat.R

class ChatHistoryAdapter(private var context: Context, private var history: List<ChatHistory>) : RecyclerView.Adapter<ChatHistoryAdapter.ChatHistoryHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatHistoryHolder {

          val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_history, parent, false)
          return ChatHistoryHolder(view)
    }


    //2. Our Adapter needs 2 ViewHolders
    class ChatHistoryHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var titleText: TextView = itemView.findViewById(R.id.textViewChatHistoryTitle)
        private var subtitleText: TextView = itemView.findViewById(R.id.textViewChatHistorySubtitle)
        private var profileImage: ImageView = itemView.findViewById(R.id.imageViewChatHistoryProfile)


        fun bind(chatHistory: ChatHistory) {
            titleText.text = chatHistory.receiverName
            subtitleText.text = chatHistory.senderChat
            profileImage.setImageResource(chatHistory.profile)

        }

    }


    // // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: ChatHistoryHolder, position: Int) {
        val chat: ChatHistory = history[position]
        holder.bind(chat)
    }

    override fun getItemCount(): Int {
        return history.size
    }



}