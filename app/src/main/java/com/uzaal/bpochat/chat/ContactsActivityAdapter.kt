package com.uzaal.bpochat.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.usecases.entities.ChatContact
import com.example.usecases.entities.ChatUser
import com.uzaal.bpochat.R

class ContactsActivityAdapter(private var context: Context, private var history: List<ChatUser>,private var contactslistner:Contactslistner) : RecyclerView.Adapter<ContactsActivityAdapter.ChatContactsHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatContactsHolder {

          val  view = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_contacts, parent, false)
          return ChatContactsHolder(view)
    }


    //2. Our Adapter needs 2 ViewHolders
    class ChatContactsHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var titleText: TextView = itemView.findViewById(R.id.textViewChatHistoryTitle)
        private var subtitleText: TextView = itemView.findViewById(R.id.textViewChatHistorySubtitle)
        private var profileImage: ImageView = itemView.findViewById(R.id.imageViewChatHistoryProfile)




        fun bind(chatContact: ChatUser, contactslistner: Contactslistner) {
            titleText.text = chatContact.userName
            subtitleText.text = chatContact.status?.statusMsg
          //  profileImage.setImageResource(chatHistory.profile)

            val position = adapterPosition

            itemView.setOnClickListener {
                contactslistner.onContactClick(itemView,position,chatContact)

            }



        }




    }


    // // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: ChatContactsHolder, position: Int) {
        val chat: ChatUser = history[position]
        holder.bind(chat,contactslistner)
    }

    override fun getItemCount(): Int {
        return history.size
    }



    fun getContact(pos: Int): ChatUser {
        return history[pos]
    }





    //Clicklistner implementation


    interface Contactslistner{

        fun onContactClick(view:View, pos:Int,contact:ChatUser)

    }



}