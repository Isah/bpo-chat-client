package com.uzaal.bpochat.injection

import android.content.Context
import com.example.controllers.ModuleControllers
import com.example.controllers.SignUpController
import com.example.controllers.SignUpPresenter
import com.example.usecases.signup.ModuleSignUp
import com.example.usecases.signup.SignUp
import com.example.usecases.utilities.PreferenceHandler
import com.uzaal.bpochat.chat.ChatViewActivity
import com.uzaal.bpochat.signup.SignUpFragment

class MyInjection(var frag : SignUpFragment) {

    private var controller : SignUpController ? = null

    init{

        val moduleController = ModuleControllers()
        val modulePresenter  = ModulePresenters()
        val moduleSignUp  = ModuleSignUp()

        val signUp : SignUp = moduleSignUp.provideSignUpUseCase(moduleSignUp.provideSignUpRepo(moduleSignUp.provideSignUpApi(),moduleSignUp.provideSignUpService(frag.requireContext()),
            PreferenceHandler(frag.requireContext())
        ))
        val presenter : SignUpPresenter = modulePresenter.provideSignUpPresenter(frag)

        controller = moduleController.provideSignUpController(frag.requireActivity(),presenter,signUp)

    }

    fun getSignUpController():SignUpController{

        return controller!!
    }



}