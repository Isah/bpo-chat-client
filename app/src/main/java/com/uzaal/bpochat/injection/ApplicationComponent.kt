package com.uzaal.bpochat.injection

import android.content.Context
import com.example.controllers.ModuleControllers
import com.example.usecases.signup.ModuleSignUp
import com.uzaal.bpochat.signup.WelcomeFragment
import com.uzaal.bpochat.signup.SignUpFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ ModuleAndroid::class, ModuleSignUp::class, ModuleControllers::class, ModulePresenters::class,  ModuleAndroid::class])
interface ApplicationComponent {
    fun context(): Context?

    //main activity injected in context at beginning
    fun inject(c : Context)

    //inject signup controller in fragment
    fun inject(frag: SignUpFragment)

    fun inject(welcome: WelcomeFragment)


    //


    //inject use cases into view model factory
  //  fun inject(messageActivityViewModelFactory: MessageActivityViewModelFactory?)

    //inject application into the preference handler
   // fun inject(preferenceHandler: PreferenceHandler?)
}