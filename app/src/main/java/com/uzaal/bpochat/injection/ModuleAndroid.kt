package com.uzaal.bpochat.injection

import android.app.Application
import android.content.Context
import com.uzaal.bpochat.MainActivity
import com.uzaal.bpochat.utilities.AppExecutors
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModuleAndroid(private val context: Context, private val application: Application) {



    @Provides
    fun provideAppExecutors(): AppExecutors {
        return AppExecutors()
    }



    @Provides
    fun provideContext(): Context {
        return context
    }

    @Provides
    fun provideApplicationContext(): Application {
        return application
    }


    fun provideSignUpView(app : MainActivity){


        return

    }


}