package com.uzaal.bpochat.injection

import com.example.controllers.ChatsPresenter
import com.example.controllers.SignUpPresenter
import com.example.presenters.ChatView
import com.example.presenters.ChatsPresenterImpl
import com.example.presenters.SignUpPresenterImpl
import com.example.presenters.SignUpView
import com.uzaal.bpochat.signup.SignUpFragment
import dagger.Module
import dagger.Provides

@Module
class ModulePresenters {

    //isia 54:5

    @Provides
    fun provideSignUpPresenter(signupView: SignUpView): SignUpPresenter {
        return SignUpPresenterImpl(signupView)
    }

    @Provides
    fun provideChatsPresenter(chatView: ChatView): ChatsPresenter {
        return ChatsPresenterImpl(chatView)
    }

    @Provides
    fun provideSignUpView() : SignUpView{
        val fragment = SignUpFragment()
     //   val args = Bundle()
      //  args.putString(com.vims.vimsapp.view.reports.FragmentReportsHome.ARG_PARAM1, param1)
     //   args.putString(com.vims.vimsapp.view.reports.FragmentReportsHome.ARG_PARAM2, param2)
     //   fragment.setArguments(args)
        return fragment

    }






}