package com.uzaal.bpochat

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.uzaal.bpochat.chat.ChatsHistoryActivity
import com.uzaal.bpochat.signup.SessionManager
import com.uzaal.bpochat.signup.WelcomeFragment


class MainActivity : AppCompatActivity() {


  //  @Inject var fragmentInjector: DispatchingAndroidInjector<Fragment>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //MyApplication.graph.inject(this)

        val sessionManager = SessionManager(this)

        if(sessionManager.isSignedUp()){
             if(sessionManager.isLoggedIn()){

                openChatsHistory()

             }else{
               //present the login screen

             }

        }else{

            openSignUp()


        }

        // look at the fields with annotation , look into the component
        // find the object of the types create and set them to the fields


    }



    private fun openSignUp(){

        var frag  = WelcomeFragment.newInstance()

        val hfragmentManager: FragmentManager = supportFragmentManager
        hfragmentManager.beginTransaction()
            .replace(R.id.signup_fragment, frag)
            .commit()

    }

    private fun openLogin(){

        var frag  = WelcomeFragment.newInstance()

        val hfragmentManager: FragmentManager = supportFragmentManager
        hfragmentManager.beginTransaction()
            .replace(R.id.signup_fragment, frag)
            .commit()

    }




    private fun openChatsHistory(){

            val intent = Intent(this, ChatsHistoryActivity::class.java)
            startActivity(intent)
            finish()

    }







}
