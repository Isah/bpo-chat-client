package com.example.presenters

import com.example.controllers.ChatsPresenter
import com.example.usecases.chat.GetChats

class ChatsPresenterImpl(val chatsView: ChatView) : ChatsPresenter {


    override fun presentChats(result: GetChats.ResponseValue) {

         val chatViewModel = ChatViewModel(result.chats,"today","petr")
         chatsView.showChats(chatViewModel)

    }

    override fun presentNoChats() {

    }



}