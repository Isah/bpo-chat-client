package com.example.presenters

import com.example.controllers.SignUpPresenter
import com.example.usecases.signup.SignUp
import javax.inject.Inject

class SignUpPresenterImpl@Inject constructor(val signUpView: SignUpView): SignUpPresenter {



    //What here th epresenter does is change the state of the viewmodel
    //then call the view to show the state

    var viewModel = SignUpViewModel()




    override fun presentVerification(verifyMsg: String) {


            viewModel.verificationMsg = verifyMsg
            viewModel.isSigned = false

            signUpView.showSignUpView(viewModel)



    }




    override fun presentInValidInput(msg: String) {

        viewModel.invalidMsg = msg
        signUpView.showInvalidInput(viewModel)
    }



    override fun showProgress(isProgress: Boolean) {

        //Just change the state of the viewmodel object
        viewModel.isLoadProgess = isProgress
        signUpView.showProgress(viewModel)

    }

    override fun presentSignUpWelcome(response: SignUp.ResponseValue)  {

           //update theviewmodel as according to one thing

           val welcome = "Welcome:${response.chatUser!!.userName} your email:${response.chatUser!!.email}"

           val viewModel = SignUpViewModel()
           viewModel.user = response.chatUser
           viewModel.welcomeMsg = welcome
           viewModel.isSigned = true


           signUpView.showSignUpView(viewModel)

    }


    override fun presentError(msg: String) {



    }



}