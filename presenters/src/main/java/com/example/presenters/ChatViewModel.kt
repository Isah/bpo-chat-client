package com.example.presenters

import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser
import java.util.*

data class ChatViewModel(val chats:List<BpoChat>, val lastSeen: String, val user:String)



