package com.example.presenters

import com.example.usecases.entities.ChatUser

class SignUpViewModel{

    var welcomeMsg:String ? = null

    var invalidMsg:String ? = null

    var verificationMsg:String ? = null

    var isLoadProgess:Boolean = false


    var isSigned:Boolean = false


    var user: ChatUser ? = null


}