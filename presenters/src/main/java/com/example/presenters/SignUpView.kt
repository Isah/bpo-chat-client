package com.example.presenters

interface SignUpView {




    fun showProgress(signUpViewModel: SignUpViewModel)
    fun showInvalidInput(signUpViewModel: SignUpViewModel)

    fun showSignUpView(signUpViewModel: SignUpViewModel)


}