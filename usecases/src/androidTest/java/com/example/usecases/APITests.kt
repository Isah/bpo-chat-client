package com.example.usecases

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.usecases.entities.ChatUser
import com.example.usecases.signup.SignUpApi
import io.reactivex.subscribers.TestSubscriber
import junit.framework.Assert
import okhttp3.ResponseBody


import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import retrofit2.Response

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class APITests {


    lateinit var  api : SignUpApi

    @Before
    fun setup(){

        api = APIClient().getRetrofitInstance()!!.create(SignUpApi::class.java)

    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.usecases.test", appContext.packageName)
    }




    @Test
    fun shouldReceiveTokenIfRequested(){

        val subscriber: TestSubscriber<String> = TestSubscriber()

        val observable = api.requestKey("758591842")

        observable.subscribe{ item: Response<String> ->

            val str = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoiMjIzMyIsImlhdCI6MTU4NDg2MTUwOSwiZXhwIjoxNTg0ODYyMTA5fQ.JB7_3LmrAcN1u6H5lhDydwuj1oSfyaV3uPpstezlSvZ36N_AQrYoXRDMM7S6PTA1ljbNPXrab9vgNlzDMpzbnA"
            val xcr = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJzb2Z0dGVrSldUIiwic3ViIjoiMjIzMyIsImlhdCI6MTU4NDg2MTUwOSwiZXhwIjoxNTg0ODYyMTA5fQ.JB7_3LmrAcN1u6H5lhDydwuj1oSfyaV3uPpstezlSvZ36N_AQrYoXRDMM7S6PTA1ljbNPXrab9vgNlzDMpzbnA"
            val header = item.body()
          //  val headers = header.split(" ")

            //Ex,Ac
          //  Assert.assertEquals(str, headers[1])

        }
    }



    @Test
    fun register(){

        val observable = api.requestRegister(ChatUser("petr","0788654","wc"))

        observable.subscribe { item: Response<ChatUser> ->

            val yes : Boolean = false

            Assert.assertTrue(item.isSuccessful)

            //Ex,Ac
           //Assert.assertEquals(str, headers[1])

        }
    }





}
