package com.example.usecases

interface UseCase <P : UseCase.RequestValue,R: UseCase.ResponseValue> {

    interface OutputBoundary<R>{

        fun onSuccess(result: R)
        fun onNetworkFail(e:String)
        fun onInvalidMessage(e:String)
    }

    fun execute(request : P, delivery : OutputBoundary<R>)


    interface RequestValue
    interface ResponseValue

}