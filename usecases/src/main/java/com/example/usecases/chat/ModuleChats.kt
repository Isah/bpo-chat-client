package com.example.usecases.chat

import android.app.Application
import android.content.Context
import com.example.usecases.ChatDatabase
import dagger.Module
import dagger.Provides

@Module
class ModuleChats {


        @Provides
        fun provideSendChatUseCase(chatRepo: ChatRepository): SendChat {
            return SendChat(chatRepo)
        }

       @Provides
        fun provideGetChatsUseCase(chatRepo: ChatRepository): GetChats {
           return GetChats(chatRepo)
         }


        @Provides
        fun provideChatRepository(chatDao: ChatDao, chatService: ChatAndroidServiceClient, readChatService: ReadChatAndroidServiceClient): ChatRepository {
            return ChatRepository(chatDao,chatService,readChatService)
        }


        @Provides
        fun provideChatDao(app: Application): ChatDao? {
         val chatDatabase = ChatDatabase.getInstance(app)
         return   chatDatabase!!.getChatDao()
         }

         @Provides
         fun provideChatServiceClient(context: Context): ChatAndroidServiceClient {
            return ChatAndroidServiceClient(context)
         }


    @Provides
    fun provideReadChatServiceClient(context: Context): ReadChatAndroidServiceClient {
        return ReadChatAndroidServiceClient(context)
    }


}