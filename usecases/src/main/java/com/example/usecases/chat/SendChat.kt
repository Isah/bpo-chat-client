package com.example.usecases.chat

import com.example.usecases.UseCase
import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser
import com.example.usecases.utilities.AppExecutors
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class SendChat(val chatRepo: ChatRepository) : UseCase<SendChat.RequestValue,SendChat.ResponseValue>{


    override fun execute(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>) {

         //1 ask the repo to send the message observe for a success
        chatRepo.sendMessage(request.chat, object: ChatDataSource.SendMessageCallback{

            override fun onMessageSent() {
                //we save the message
                chatRepo.saveMessage(request.chat)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<Int>{

                        override fun onComplete() {}
                        override fun onError(e: Throwable) {}
                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(t: Int) {
                            if(t>0){
                             delivery.onSuccess(ResponseValue(true))
                            }
                        }


                    })
            }

            override fun onMessageFailed() {

                delivery.onNetworkFail("")
            }

        })

    }



    data class RequestValue(val chatUser: ChatUser,val chat: BpoChat):UseCase.RequestValue
    data class ResponseValue(val isSent: Boolean): UseCase.ResponseValue


}