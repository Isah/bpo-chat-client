package com.example.usecases.chat

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.usecases.entities.BpoChat
import io.reactivex.Observable


@Dao
interface ChatDao {



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveChat(chat: BpoChat) : Observable<Int>

    @Query("SELECT * FROM bpochat WHERE messageId = :id")
    fun getChatById(id: String?): BpoChat?


    @Query("SELECT * FROM bpochat WHERE messageId = :id")
    fun getChatsById(id: String?): Observable<List<BpoChat>>



}