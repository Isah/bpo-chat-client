package com.example.usecases.chat

import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser

interface ChatDataSource {

    //sending message
    interface SendMessageCallback {
        fun onMessageSent()
        fun onMessageFailed()
    }

    fun sendMessage(bpoChat: BpoChat, sendMessageCallback: SendMessageCallback)


    //get messagaes

    //get messagaes
    interface GetMessageCallback {
        fun onMessageLoaded(bpoChat: BpoChat)
        fun onDataNotAvailable()
    }


    fun getMessage(chatUser: ChatUser, getMessageCallback: GetMessageCallback)

}