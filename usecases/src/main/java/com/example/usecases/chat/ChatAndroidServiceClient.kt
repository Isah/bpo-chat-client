package com.example.usecases.chat

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.example.usecases.entities.BpoChat
import com.example.usecases.config.HostConfig

import javax.inject.Inject

class ChatAndroidServiceClient @Inject constructor(var myContext: Context) : ChatAndroidService.OnConnectionEstablishedListener{



    private var binderService: ChatAndroidService? = null
    var mBound = false
    var isConnected = false
    var isSent = false



    fun bindtoLocalServiceAndSend(bpoChat: BpoChat, dataSource: ChatDataSource.SendMessageCallback){ // myContext =  preferenceHandler.getActivityContext();
        // Bind to LocalService
      //  val intent = Intent(myContext, com.uzaal.bpochat.signup.RegisterSmackService::class.java)
        // User user = HostConfig.getUser(myContext);

        Log.d("Bound Service Before", "" + myContext.packageName)

        // application.startService(intent);
        myContext.bindService(null, object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) { //LocalBinder binder = (LocalBinder) iBinder;
                binderService = (iBinder as ChatAndroidService.LocalBinder).getService()
                Log.d("Bound Service Conn", "" + binderService!!.started)
                //binderService = (MyXMPPBinderService) binder.getService();
                if (binderService != null) {
                    Log.d("service-bind", "Service is bonded successfully!")
                    mBound = true
                    binderService!!.setOnConnectionEstablishedListener(this@ChatAndroidServiceClient)
                    //do whatever you want to do after successful binding


                    binderService!!.sendMessage(bpoChat,dataSource)


                } else {
                    Log.d("service-bind", "Service is not bonded !")
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName) {


            }

        }, Context.BIND_AUTO_CREATE)
        Log.d("Bound Service After", "" + myContext.packageName)

    }


    override fun onConnectionEstablished() {

        isConnected = true

    }






}