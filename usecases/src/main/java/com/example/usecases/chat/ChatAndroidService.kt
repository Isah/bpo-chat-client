package com.example.usecases.chat

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.util.Log
import androidx.annotation.RestrictTo
import com.example.usecases.entities.BpoChat
import com.example.usecases.signup.ConnectionConfig
import com.example.usecases.utilities.AppExecutors
import io.reactivex.Observable
import org.jivesoftware.smack.SmackException.NotConnectedException
import org.jivesoftware.smack.chat2.ChatManager
import org.jivesoftware.smack.chat2.IncomingChatMessageListener
import org.jivesoftware.smack.packet.Message
import org.jxmpp.jid.EntityBareJid
import org.jxmpp.jid.impl.JidCreate
import org.jxmpp.stringprep.XmppStringprepException
import java.util.*

internal class ChatAndroidService : Service() {
    private var userName: String? = null
    private var passWord: String? = null
    private val message: String? = null
    private val from: String? = null
    private val to: String? = null
    private var HOST: String? = null
    private var HOST_ADDRESS: String? = null
    // private ChatXmpp myXMPP = ChatXmpp.getInstance();
    private val appExecutors: AppExecutors = AppExecutors()


    @RestrictTo(RestrictTo.Scope.TESTS)
    var started = false
    @RestrictTo(RestrictTo.Scope.TESTS)
    var stopped = false
    @RestrictTo(RestrictTo.Scope.TESTS)
    var isOnStartCommandStarted = false



    //1. Binder given to clients to bind to services
    private val mBinder: IBinder = LocalBinder()

    /**2
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods

        fun getService(): ChatAndroidService? { // Return this instance of LocalService so clients can call public methods
            return this@ChatAndroidService
        }
    }


    // To provide binding for a service i need to implement onBind()
// returns a IBinder that provides a programming interface that clients
// can use to interact with the service
    override fun onBind(intent: Intent): IBinder {

        passWord = intent.getStringExtra("pwd")
        userName = intent.getStringExtra("usr")
        HOST = intent.getStringExtra("host")
        HOST_ADDRESS = intent.getStringExtra("host_address")
        Log.d("SERVICE CONN BOUND: ", "time: " + Date())

        return mBinder
    }

    private var mIsConnectionEstablished = false

    interface OnConnectionEstablishedListener {
        fun onConnectionEstablished()
    }

    private var mListener: OnConnectionEstablishedListener? = null
    //the one that impl this interface will get the call back
    fun setOnConnectionEstablishedListener(listener: OnConnectionEstablishedListener?) {
        mListener = listener
        // Already connected to server. Notify immediately.
        if (mIsConnectionEstablished) {
            mListener!!.onConnectionEstablished()
        }
    }

    private fun notifyConnectionEstablished() {
        mIsConnectionEstablished = true
        if (mListener != null) {
            mListener!!.onConnectionEstablished()
        }
    }

    override fun onCreate() {
        super.onCreate()
        started = true
        val handler = Handler()
        val runnable = Runnable {
            Log.d("SERVICE CONN EST: ", "time: " + Date())
            notifyConnectionEstablished()
        }
        handler.postDelayed(runnable, 5000)
        appExecutors.diskIO().execute(runnable)
    }

    override fun onDestroy() { // myXMPP.disconnectConnection();
        super.onDestroy()
        stopped = true
    }

    var myXMPPConnection: ConnectionConfig? = ConnectionConfig.instance
    lateinit var chatmanager: ChatManager


    fun sendMessage(bpochat: BpoChat, dataSource: ChatDataSource.SendMessageCallback){ //This is where the retrofit or xmpp msg client would come in

        Log.d("MSG SEND", "ACTIVE")

        //val obs : Observable<BpoChat>

        var entityBareJid: EntityBareJid? = null
        try {
            entityBareJid = JidCreate.entityBareFrom(bpochat.to)
        } catch (x: XmppStringprepException) {
        }

        val message = Message()
        message.body = bpochat.message
        message.subject = "Conversation"
        message.to = entityBareJid


        try {
            chatmanager = ChatManager.getInstanceFor(myXMPPConnection!!.tcpConnection)
            Log.d("MSG SEND", "IN QUEUE")
            try {
                //  EntityJid my_entity_jid =  mConnection.getUser();
                val my_entity_jid = message.to.asEntityBareJidIfPossible()

                Log.d("MSG SEND", "IN ENTITY JID $my_entity_jid")

                val newChat = chatmanager.chatWith(my_entity_jid)

                  newChat.send(message)

                  dataSource.onMessageSent()


            } catch (e: NotConnectedException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        } catch (e: Exception) {

        }

    }




}