package com.example.usecases.chat

import com.example.usecases.UseCase
import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser
import com.example.usecases.utilities.AppExecutors
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class GetChats(val chatRepo: ChatRepository) : UseCase<GetChats.RequestValue,GetChats.ResponseValue>{



    companion object{
        val LIST_CHATS = 1
        val CHAT = 0

    }

    override fun execute(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>) {


        if(request.chat_choice == LIST_CHATS){

            getChatsFromDb(request,delivery)
        }else{

            readIncomingChat(request,delivery)

        }
         //1 ask the repo to send the message observe for a success
          // Get chats from DB
    }

    private  fun getChatsFromDb(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>){

        chatRepo.getChats(request.chatUser.userName!!)
            .subscribeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<List<BpoChat>>{

                override fun onNext(t: List<BpoChat>) {

                    delivery.onSuccess(ResponseValue(t))
                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {

                }

                override fun onSubscribe(d: Disposable) {

                }


            })

    }

    private fun readIncomingChat(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>){

        chatRepo.getMessage(request.chatUser,object: ChatDataSource.GetMessageCallback{

            override fun onMessageLoaded(bpoChat: BpoChat) {
                //save it in the database
                chatRepo.saveMessage(bpoChat)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Observer<Int>{

                        override fun onComplete() {}
                        override fun onError(e: Throwable) {}
                        override fun onSubscribe(d: Disposable) {}
                        override fun onNext(t: Int) {
                            if(t>0){

                                val arr = ArrayList<BpoChat>()
                                delivery.onSuccess(ResponseValue(arr))

                            }
                        }

                    })

            }

            override fun onDataNotAvailable() {
            }

        })


    }





    data class RequestValue(val chat_choice:Int, val chatUser:ChatUser):UseCase.RequestValue
    data class ResponseValue(val chats: List<BpoChat>): UseCase.ResponseValue


}