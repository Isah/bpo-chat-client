package com.example.usecases.chat

import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser
import io.reactivex.Observable
import java.util.*
import kotlin.collections.ArrayList


class ChatRepository(val chatDao: ChatDao, val chatService: ChatAndroidServiceClient, val readChatService: ReadChatAndroidServiceClient):ChatDataSource {


    fun getChats(userId: String): Observable<List<BpoChat>>{

        val bpoChat1 = BpoChat("1","12345","0758591842","Hello Mr",1,2,
            Date(),"henry@localhost",userId)

        val bpoChat2 = BpoChat("2","12346","0758591842","Hello General",1,2,
            Date(),"henry@localhost",userId)

        val list = ArrayList<BpoChat>()


      return  Observable.just(list)

    }


    override fun sendMessage(bpoChat: BpoChat, sendMessageCallback: ChatDataSource.SendMessageCallback) {

        //to server
        chatService.bindtoLocalServiceAndSend(bpoChat,sendMessageCallback)

    }

    override fun getMessage(chatUser: ChatUser, getMessageCallback: ChatDataSource.GetMessageCallback) {
         //from server
          readChatService.bindtoLocalServiceAndReadIncomingMsg(chatUser,getMessageCallback)

    }

    fun getMessages(userId: String) : Observable<List<BpoChat>> {
         //from db
        val obs = chatDao.getChatsById(userId)

        return obs

    }

     fun saveMessage(bpoChat: BpoChat): Observable<Int>{
         //to db
        return chatDao.saveChat(bpoChat)
    }




}