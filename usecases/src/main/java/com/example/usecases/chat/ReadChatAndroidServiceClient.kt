package com.example.usecases.chat

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.example.usecases.entities.ChatUser
import com.example.usecases.config.HostConfig

import javax.inject.Inject

class ReadChatAndroidServiceClient @Inject constructor(var myContext: Context) : ChatAndroidService.OnConnectionEstablishedListener,
    ReadChatAndroidService.OnConnectionEstablishedListener {



    private var binderService: ReadChatAndroidService? = null
    var mBound = false

    var isConnected = false

    var isSent = false



    fun bindtoLocalServiceAndReadIncomingMsg(chatUser: ChatUser, dataSource: ChatDataSource.GetMessageCallback){ // myContext =  preferenceHandler.getActivityContext();
        // Bind to LocalService
        /*
        val intent = Intent(myContext, com.uzaal.bpochat.signup.RegisterSmackService::class.java)
        // User user = HostConfig.getUser(myContext);
        intent.putExtra("usr", chatUser.userName)
        intent.putExtra("pwd", chatUser.phoneNumber)
        intent.putExtra("host", HostConfig.HOST)
        intent.putExtra("host_address", HostConfig.HOST_ADDRESS)
        Log.d("Bound Service Before", "" + myContext.packageName)

         */

        // application.startService(intent);
        myContext.bindService(null, object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) { //LocalBinder binder = (LocalBinder) iBinder;
                binderService = (iBinder as ReadChatAndroidService.LocalBinder).getService()
                Log.d("Bound Service Conn", "" + binderService!!.started)
                //binderService = (MyXMPPBinderService) binder.getService();
                if (binderService != null) {
                    Log.d("service-bind", "Service is bonded successfully!")
                    mBound = true
                    binderService!!.setOnConnectionEstablishedListener(this@ReadChatAndroidServiceClient)
                    //do whatever you want to do after successful binding


                    binderService!!.getMessage(chatUser, dataSource)

                } else {
                    Log.d("service-bind", "Service is not bonded !")
                }
            }

            override fun onServiceDisconnected(componentName: ComponentName) {


            }

        }, Context.BIND_AUTO_CREATE)
        Log.d("Bound Service After", "" + myContext.packageName)

    }


    override fun onConnectionEstablished() {

        isConnected = true

    }









}