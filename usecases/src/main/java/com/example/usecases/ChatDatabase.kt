package com.example.usecases

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.usecases.chat.ChatDao
import com.example.usecases.entities.BpoChat
import com.example.usecases.utilities.DateConverter


@Database(
    entities = [BpoChat::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class ChatDatabase : RoomDatabase() {


    companion object {


        // For Singleton instantiation
        private val LOCK = Any()
        private var sInstance: ChatDatabase? = null


        private val DATABASE_NAME = "bpochats"


        fun getInstance(application: Application?): ChatDatabase? {
            // Log.d(LOG_TAG, "Getting the database")
            if (sInstance == null) {
                synchronized(LOCK) {
                    sInstance = Room.databaseBuilder(
                        application!!,
                        ChatDatabase::class.java, DATABASE_NAME
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    // Log.d(LOG_TAG, "Made new database")
                }
            }
            return sInstance
        }




    }


    abstract fun getChatDao(): ChatDao?



}




