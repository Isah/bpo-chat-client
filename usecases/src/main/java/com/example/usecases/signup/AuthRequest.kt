package com.example.usecases.signup

data class AuthRequest (val username:String?, val password:String?)