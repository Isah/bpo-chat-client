package com.example.usecases.signup

class SignUpRequest {

    var username: String? = null
    var password: String? = null
    var phoneNumber:String? = null
    var activation_code:String ? = null
    var email:String? = null

}