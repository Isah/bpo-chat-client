package com.example.usecases.signup

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import android.util.Log
import com.example.usecases.config.HostConfig
import com.example.usecases.entities.ChatUser
import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/***
 * Job: Execute the  BinderService tasks
 * 1. start the service
 * 2. invoke login, send msg, get msg
 * 3. all those invocations happen after a connection has been established
 * that's why we have the connection listener
 */

class RegisterSmackServiceClient @Inject constructor(var myContext: Context) {

    //my intent service needs Myxmpp
   // private lateinit var binderService: com.uzaal.bpochat.signup.RegisterSmackService
    var mBound = false

    var isConnected = false


    fun bindtoLocalService(userName: String?, pass: String?): Observable<ChatUser> { // myContext =  preferenceHandler.getActivityContext();
       // Bind to LocalService
        /*
        val intent = Intent(myContext, com.uzaal.bpochat.signup.RegisterSmackService::class.java)
        // User user = HostConfig.getUser(myContext);
        intent.putExtra("usr", userName)
        intent.putExtra("pwd", pass)
        intent.putExtra("host", HostConfig.HOST)
        intent.putExtra("host_address", HostConfig.HOST_ADDRESS)
        /
         */
        Log.d("Bound Service Before", "" + myContext.packageName)

        // application.startService(intent);
        myContext.bindService(null, object : ServiceConnection {
            override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) { //LocalBinder binder = (LocalBinder) iBinder;

                Log.d("Bound Service Conn", "Inside")

                //why don't i just register the guy here ,but again how will i receive back the registration

             //   binderService = (iBinder as com.uzaal.bpochat.signup.RegisterSmackService.LocalBinder).getService()
             //   Log.d("Bound Service Conn", "" + binderService.started)
                //binderService = (MyXMPPBinderService) binder.getService();

                if(mBound){

                    Log.d("Bound Service Conn", "IsBound:" + mBound)
                }

                /*
                if (binderService != null) {
                    Log.d("service-bind", "Service is bonded successfully!")
                    mBound = true
                    binderService.setOnConnectionEstablishedListener(this@RegisterSmackServiceClient)
                } else {
                    Log.d("service-bind", "Service is not bonded !")
                }
                /
                 */



            }

            override fun onServiceDisconnected(componentName: ComponentName) {


            }

        }, Context.BIND_AUTO_CREATE)
        Log.d("Bound Service After", "" + myContext.packageName)

        if (mBound){
        //  return  register(userName,pass)
        }

        return Observable.empty()

    }


    /*
    override fun onConnectionEstablished() {

      isConnected = true

    }

     */


    fun bind(userName: String?, password: String?) {
        if (!mBound) {
            bindtoLocalService(userName, password)
        }
    }


    /** Defines callbacks for service binding, passed to bindService()  */
    /*
    fun register(userName: String?, password: String?): Observable<ChatUser> {
        if (!isConnected) {

         return  binderService.registerUser(userName!!,password!!)
             .delay(6, TimeUnit.SECONDS)

        }

        return Observable.empty()
    }

     */



}