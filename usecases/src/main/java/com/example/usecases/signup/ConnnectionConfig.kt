package com.example.usecases.signup

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import org.jivesoftware.smack.*
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import org.jivesoftware.smack.util.TLSUtils
import org.jivesoftware.smackx.ping.PingFailedListener
import org.jivesoftware.smackx.ping.PingManager
import org.jxmpp.jid.DomainBareJid
import org.jxmpp.jid.EntityFullJid
import org.jxmpp.jid.impl.JidCreate
import org.jxmpp.stringprep.XmppStringprepException
import java.io.IOException
import java.net.InetAddress
import java.net.UnknownHostException
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException


class ConnectionConfig private constructor() {

    //Stores whether or not the thread is active
    private var mActive = false
    private var mThread: Thread? = null
    //We use this handler to post messages to
    private var mTHandler: Handler? = null


    //The background thread.
    private val userName = ""
    private val passWord = ""
    var connectionListener = MyXMPPConnectionListener()
    var isToasted = false
    var chat_created = false
    var loggedin = false

    var inetAddress: InetAddress? = null

    var tcpConnection: XMPPTCPConnection? = null

    private  val config: XMPPTCPConnectionConfiguration.Builder = XMPPTCPConnectionConfiguration.builder()
    var serviceName: DomainBareJid? = null

    private var mUserName: String? = null
    private var mPassWord: String? = null



    private fun configureConnection(host: String?, hostAddress: String?):XMPPTCPConnectionConfiguration { //  this.userName = mUsername; //  this.passWord = mPassword;
        Log.i(TAG, "Initialisation")
       // config = XMPPTCPConnectionConfiguration.builder()
        try {
            serviceName = JidCreate.domainBareFrom(host)
            inetAddress = InetAddress.getByName(hostAddress)
        } catch (e: UnknownHostException) {
        } catch (e: XmppStringprepException) {
        }
        // config.setServiceName(DOMAIN); //username of the pc
        config.setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible)
        config.setXmppDomain(serviceName)
        config.setHostAddress(inetAddress)
        config.setPort(PORT)
        config.setCompressionEnabled(false)
        config.setDebuggerEnabled(true)
        config.setSendPresence(true)
        config.setConnectTimeout(15000)
       // config.setUsernameAndPassword(mUserName, mPassWord)
        //XMPPTCPConnection.setUseStreamManagementResumptiodDefault(true);
        //XMPPTCPConnection.setUseStreamManagementDefault(true);
        try {
            TLSUtils.acceptAllCertificates(config)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
            e.printStackTrace()
        }


        return config.build()

      //  startConnecting()
    }

    //PING //////////////////////////////////////////////////////////////
    inner class PingListner : PingFailedListener {
        override fun pingFailed() {
            Log.d("Png Failed", "pinging failed")
            //  assertEquals(expectedUserJid, packet.getFrom().asBareJid().toString());
        }
    }


    fun getUser(): EntityFullJid? {
        return if (isConnected) {
            tcpConnection?.user
        } else {
            null
        }
    }





    fun getConnection():XMPPTCPConnection?{

        return tcpConnection
    }


//Connection Listener to check connection state
    inner class MyXMPPConnectionListener : ConnectionListener {
        override fun connected(connection: XMPPConnection) {
            Log.d("XMPP CONNECTION LISTNER", "Connected!")
            connected = true
            if (!connection.isAuthenticated) { // login();
            }
        }

        override fun connectionClosed() {
            if (isToasted) Handler(Looper.getMainLooper()).post {
                // TODO Auto-generated method stub
            }
            Log.d("xmpp", "ConnectionCLosed!")
            connected = false
            chat_created = false
            loggedin = false
        }

        override fun connectionClosedOnError(arg0: Exception) {
            if (isToasted) Handler(Looper.getMainLooper()).post { }
            Log.d("xmpp", "ConnectionClosedOn Error!")
            connected = false
            chat_created = false
            loggedin = false
        }

        override fun reconnectingIn(arg0: Int) {
            Log.d("xmpp", "Reconnectingin $arg0")
            loggedin = false
        }

        override fun reconnectionFailed(arg0: Exception) {
            if (isToasted) Handler(Looper.getMainLooper()).post { }
            Log.d("xmpp", "ReconnectionFailed!")
            connected = false
            chat_created = false
            loggedin = false
        }

        override fun reconnectionSuccessful() {
            if (isToasted) Handler(Looper.getMainLooper()).post {
                // TODO Auto-generated method stub
            }
            Log.d("xmpp", "ReconnectionSuccessful")
            connected = true
            chat_created = false
            loggedin = false
        }

        override fun authenticated(arg0: XMPPConnection, arg1: Boolean) {
            Log.d("xmpp", "Authenticated!")
            loggedin = true
            chat_created = false
            Thread(Runnable {
                try {
                    Thread.sleep(500)
                } catch (e: InterruptedException) { // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            }).start()
            if (isToasted) Handler(Looper.getMainLooper()).post {
                // TODO Auto-generated method stub
            }
        }
    }




    /// New Connection Class
     fun getMyConnection(host: String?, hostAddress: String?): XMPPTCPConnection? {
        // Log.D(TAG, "Getting XMPP Connect")
        if (isConnected) {
             Log.d(TAG, "Returning already existing connection")
            return this.tcpConnection
        }
        val l = System.currentTimeMillis()

        try {
            if (this.tcpConnection != null) {
                //  Log.logDebug(TAG, "Connection found, trying to connect")
                this.tcpConnection?.connect()
            } else {
                Log.d(TAG, "No Connection found, trying to create a new connection")


                //config
                val config: XMPPTCPConnectionConfiguration = configureConnection(host,hostAddress)

                SmackConfiguration.DEBUG = true
                //tcp connection
                tcpConnection = XMPPTCPConnection(config)
                //add listner
                tcpConnection!!.addConnectionListener(connectionListener)
                //connect
                this.tcpConnection?.connect()
                //recconection

               //   val reconnectionManager = ReconnectionManager.getInstanceFor(tcpConnection)
              //  reconnectionManager.enableAutomaticReconnection()
              ///  ReconnectionManager.setEnabledPerDefault(true)
              //  PingManager.getInstanceFor(tcpConnection).registerPingFailedListener(PingListner())

                /*
                val config: XMPPTCPConnectionConfiguration = buildConfiguration()
                SmackConfiguration.DEBUG = true
                this.tcpConnection = XMPPTCPConnection(config)
                this.tcpConnection?.connect()
                 */

            }
        } catch (e: java.lang.Exception) {
            Log.e(TAG, "some issue with getting connection :" + e.message)
        }
        Log.d(TAG, "Connection Properties: " + tcpConnection?.host.toString() + " " + tcpConnection?.serviceName
        )
        Log.d(
            TAG,
            "Time taken in first time connect: " + (System.currentTimeMillis() - l)
        )
        return this.tcpConnection
    }


    fun getInstance(): ConnectionConfig? {
        if (instance == null) {
            synchronized(ConnectionConfig::class.java) {
                if (instance == null) {
                    instance = ConnectionConfig()
                }
            }
        }
        return instance
    }


    @Throws(
        XMPPException::class,
        SmackException::class,
        IOException::class,
        InterruptedException::class
    )
    fun login(user: String?, pass: String?, username: String?) {
        Log.d(TAG, "inside XMPP getlogin Method")
        var l = System.currentTimeMillis()
        val connect = getConnection()

        if (connect!!.isAuthenticated) {
            Log.d(TAG, "User already logged in")
            return
        }
        Log.d(TAG, "Time taken to connect: " + (System.currentTimeMillis() - l))
        l = System.currentTimeMillis()

        try {
            connect.login(user, pass)
        } catch (e: java.lang.Exception) {
            Log.e(TAG, "Issue in login, check the stacktrace")
            e.printStackTrace()
        }
        Log.i(
            TAG,
            "Time taken to login: " + (System.currentTimeMillis() - l)
        )
        Log.i(TAG, "login step passed")
        val pingManager = PingManager.getInstanceFor(connect)
        pingManager.pingInterval = 5000
    }


    val isConnected: Boolean get() = tcpConnection != null && tcpConnection!!.isConnected

    //start connection and login used by my_xmpp_service
    private fun startConnecting(): Boolean {
        Log.d(TAG, " StartConnection function called.")
        if (!mActive) {
            mActive = true
            if (mThread == null || !mThread!!.isAlive) {
                mThread = Thread(Runnable {
                    Looper.prepare()
                    mTHandler = Handler()
                    // Create a connection
                    if (isConnected) {
                        Log.d(
                            TAG,
                            "Returning already existing connection"
                        )
                    } else {
                        try {
                            Log.d(
                                TAG,
                                "NO CONNECTION FOUND, trying to create a new connection"
                            )
                            SmackConfiguration.DEBUG = true
                            tcpConnection!!.connect()
                            Log.d(
                                TAG,
                                "CONNECTION CREATED " + tcpConnection!!.streamId
                            )
                            connected = true
                        } catch (e: IOException) {
                            connected = false
                            Log.i("CONNECTION EXCEPTION", "Something Happened!")
                            e.printStackTrace()
                        } catch (e: SmackException) {
                            connected = false
                            Log.i("CONNECTION EXCEPTION", "Something Happened!")
                            e.printStackTrace()
                        } catch (e: XMPPException) {
                            connected = false
                            Log.i("CONNECTION EXCEPTION", "Something Happened!")
                            e.printStackTrace()
                        } catch (e: InterruptedException) {
                            connected = false
                            Log.i("CONNECTION EXCEPTION", "Something Happened!")
                            e.printStackTrace()
                        }
                    }
                    //THE CODE HERE RUNS IN A BACKGROUND THREAD.
                    Looper.loop()
                })
                mThread!!.start()
            }
        }
        Log.d(
            TAG,
            "Connection Properties: " + tcpConnection!!.host + " " + tcpConnection!!.serviceName
        )
        return connected
    }








    fun stop() {
        Log.d(TAG, "stop()")
        mActive = false
        mTHandler!!.post(object : Runnable {
            override fun run() {
                if (tcpConnection != null) {
                    tcpConnection!!.disconnect()
                }
                //CODE HERE IS MEANT TO SHUT DOWN OUR CONNECTION TO THE SERVER.
            }
        })
    }

    companion object {
        private const val TAG = "RoosterService"
        //ca be accessed for testing
        //@VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
        var connected = false
        const val PORT = 5222
        var mApplicationContext: Context? = null
        var instance: ConnectionConfig? = null
            get() {
                if (field == null) {
                    synchronized(ConnectionConfig::class.java) {
                        if (field == null) {
                            field = ConnectionConfig()
                        }
                    }
                }
                return field
            }
            private set

       // private val dataToSend = StringUtils.randomString(1024 * 4 * 3).toByteArray()
    }
}
