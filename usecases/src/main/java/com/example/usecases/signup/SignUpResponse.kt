package com.example.usecases.signup

class SignUpResponse {

     var phoneNumber : String ? = null
     var signUpStatus : Int = 0

}