package com.example.usecases.signup

import android.util.Log
import com.example.usecases.UseCase
import com.example.usecases.entities.ChatUser
import com.example.usecases.entities.ResponseStatus
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.plugins.RxJavaPlugins.onError
import io.reactivex.schedulers.Schedulers
import okhttp3.Response
import okhttp3.ResponseBody
import rx.functions.Action1
import javax.inject.Inject
import kotlin.math.sign


open class SignUp @Inject constructor(signUpRepo: SignUpRepository) : UseCase<SignUp.RequestValue,SignUp.ResponseValue>{

   var mSignUpRepo:SignUpRepository = signUpRepo

    //Inject the SignUp Repository
    //Used also to test

    companion object{
        lateinit  var  responseValue : ResponseValue
        val RIGISTER : Int = 1
        val ACTIVATE : Int = 2
        val ACCOUNT : Int = 3

    }

    override fun execute(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>) {

        if(request.task == RIGISTER){

            register(request,delivery)

        }else if (request.task == ACTIVATE){
            //activate
            activate(request,delivery)
        }else{
           // registerToMessenger(request,delivery)
        }

    }



    private fun activate(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>){
        //Access SignUp API

        val mSignUpRequest = SignUpRequest()
        mSignUpRequest.username = request.userName
        mSignUpRequest.password = request.password
        mSignUpRequest.phoneNumber = request.phoneNumber
        mSignUpRequest.activation_code = request.activation_code

        val observable : Observable<SignUpResponse> =  mSignUpRepo.activateUser(mSignUpRequest)
            observable.subscribe(object: DisposableObserver<SignUpResponse>() {

                override fun onNext(t: SignUpResponse) {

                    // Here the user has been registered and activated on the server
                    // so we need to save the user token and register him on our messaging server
                    // to start messaging
                    // The token helps to
                    // If status isnt null get me the token wich i know isn't null
                           //mSignUpRepo.saveToken(t.status?.token!!)

                    if (t.signUpStatus == 2){

                        registerToMessenger(mSignUpRequest,delivery)

                    }


                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {

                }


            })





    }

    private fun registerToMessenger(user:SignUpRequest,delivery: UseCase.OutputBoundary<ResponseValue>){
      //Access Messenger Library/API

      //  Log.d("MyUser",user.userName!!)

        mSignUpRepo.signUpForMessaging(user)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(object: DisposableObserver<ChatUser>() {

                override fun onNext(t: ChatUser) {

                  //  val resp = ResponseValue(t)
                 //   delivery.onSuccess(resp)

                }

                override fun onComplete() {

                }

                override fun onError(e: Throwable) {

                    delivery.onNetworkFail(e.message!!)

                }

            })

    }

    //If this is to be a facade it doesn't play with the domain business rules
    // but it can have an interface to send the results

    //after this wait for the msg
    private fun register(request: RequestValue, delivery: UseCase.OutputBoundary<ResponseValue>){
        //Access SignUp API

        if(validatePhone(request.phoneNumber,9)) {

            mSignUpRepo.registerUser(request.userName,request.password, request.phoneNumber)
                .subscribe(object:Observer<SignUpResponse>{

                    override fun onNext(signUpResponse : SignUpResponse) {

                        delivery.onSuccess(ResponseValue(signUpResponse))

                    }

                    override fun onError(e: Throwable) {
                        delivery.onInvalidMessage(e.message!!)
                    }

                    override fun onComplete() {

                    }

                    override fun onSubscribe(d: Disposable) {

                    }

                })


                    /*
                .subscribe { chatUser: ChatUser ->

                   delivery.onSuccess(ResponseValue(chatUser))


                }
                     */


        }else{

            delivery.onInvalidMessage("Phone number is InValid! len ${request.phoneNumber.length}")

        }

    }

    private fun validatePhone(phone:String,requiredSize:Int)  = phone.length == requiredSize




    data class RequestValue(val userName:String, val phoneNumber:String, val password:String, var activation_code: String, val task:Int):UseCase.RequestValue


    data class ResponseValue(val signUpResponse: SignUpResponse?):UseCase.ResponseValue


}