package com.example.usecases.signup

import android.annotation.SuppressLint
import com.example.usecases.config.HostConfig
import com.example.usecases.config.HostConfig.Companion.HOST
import com.example.usecases.config.HostConfig.Companion.HOST_ADDRESS
import com.example.usecases.entities.ChatUser
import com.example.usecases.entities.ChatUserStatus
import com.example.usecases.entities.ResponseStatus
import com.example.usecases.utilities.AppExecutors
import com.example.usecases.utilities.PreferenceHandler
import io.reactivex.Observable
import org.jivesoftware.smack.SmackException
import org.jivesoftware.smackx.iqregister.AccountManager
import org.jxmpp.jid.parts.Localpart
import java.io.IOException
import java.util.concurrent.Callable
import javax.inject.Inject


class SignUpRepository @Inject constructor(val signUpAPi: SignUpApi, val registerService: RegisterSmackServiceClient,val pref: PreferenceHandler) {


    @SuppressLint("CheckResult")
    fun registerUser(userName: String, password: String,phoneNumber: String ):Observable<SignUpResponse>{
        //1. user sends phone number,username to server api
        //2. the server generates an activation code
        //3. server saves the user details with corresponding activation code , marks the user inactive
        //4. the the server requests an sms gateway to send the code to the users phone number


        val mSignUpRequest = SignUpRequest()
        mSignUpRequest.username = userName
        mSignUpRequest.password = password
        mSignUpRequest.phoneNumber = phoneNumber

        val mSignUpResponse =  signUpAPi.requestSignUp(mSignUpRequest)

         /*
        var userStatus : ChatUserStatus ? = null
        val user: ChatUser = ChatUser(userName,password,phoneNumber,"",userStatus)
        Observable.just(user)
         */

        return mSignUpResponse

    }

    //  Testing Repositories with observables
    lateinit var userStatus : ChatUserStatus

    fun activateUser(signUpRequest: SignUpRequest): Observable<SignUpResponse>{


        //5. user recieves code and sends code to server to verify and activate the user
        //6. the server sends the token based on the phone number to the user for usage in accessing server api
             //for user details


        // will be delivered as injection
        // val api : SignUpApi = APIClient().getRetrofitInstance()!!.create(SignUpApi::class.java)
        //you observe the response then unwrap it
        //  val res = ResponseStatus(true,200,"123abc")
        // It won’t compile, if you don’t check first if it’s null:
        //if token isnt  null then do

        // val user  = ChatUser(userName,password,phoneNumber,"",userStatus)
        // val observable =   signUpAPi.requestKey(activation_code)
        // Observable.just(user)

        val res = SignUpResponse()
        res.phoneNumber = signUpRequest.phoneNumber
        res.signUpStatus = 2

        return Observable.just(res)
    }




    fun authenticateWithTwilio(token: String): Observable<String>{
        //make request to twilo API
        //then twilo will respond get the response and use twilio sdk to auth the token

         val responseFromTwilio = 2345
         //send it back for verification
         val authResponse = "success"

      return   Observable.just(authResponse)

    }


    fun getList(): Observable<ArrayList<String>>{

        val arr = ArrayList<String>()
        arr.add("0758591842")
        arr.add("2")

       return Observable.just(arr)

    }






     var myXMPPConnection: ConnectionConfig? = ConnectionConfig.instance

    fun signUpForMessaging(myuser:SignUpRequest): Observable<ChatUser> {

       return  Observable.fromCallable (object:Callable<ChatUser> {

           override fun call(): ChatUser? {

               try {
                   val accountManager = AccountManager.getInstance(
                       myXMPPConnection?.getMyConnection(
                           HostConfig.HOST, HOST_ADDRESS
                       )
                   )
                   accountManager.sensitiveOperationOverInsecureConnection(true)
                   accountManager.createAccount(Localpart.from(myuser.username), myuser.password)


                   var userMail = Localpart.from(myuser.username).toString()
                  // myuser.email = userMail


                   var chatUser = ChatUser(myuser.username)
                   chatUser.password = myuser.password
                   chatUser.email = userMail
                   chatUser.phoneNumber = myuser.phoneNumber


                   return chatUser

               } catch (e: SmackException) {
                   e.printStackTrace()


               } catch (e: IOException) {
                   e.printStackTrace()
               } catch (e: InterruptedException) {
                   e.printStackTrace()
               }


               return null
           }

       })




           /*
        val run = Runnable {

            try {
                val accountManager = AccountManager.getInstance(myXMPPConnection?.getMyConnection(
                    HostConfig.HOST, HOST_ADDRESS))
                accountManager.sensitiveOperationOverInsecureConnection(true)
                accountManager.createAccount(Localpart.from(userName), password)


                var userMail = Localpart.from(userName).toString()

                val user: ChatUser = ChatUser(userName,password,"",userMail,userStatus)



               // return Observable.just(user)

            } catch (e: SmackException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }



        }

            */
    //   val appExecutors = AppExecutors().diskIO()
      //  appExecutors.execute(run)

        //set a connection using the admin
     //   myXMPPConnection!!.setUserOnConnection("admin@localhost", "admin")
     //   myXMPPConnection?.configureConnection(HOST, HOST_ADDRESS)


    }


    fun saveToken(token:String){
        pref.saveUserToken(token)
    }


}