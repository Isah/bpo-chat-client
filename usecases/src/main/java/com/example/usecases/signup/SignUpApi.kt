package com.example.usecases.signup

import com.example.usecases.entities.ChatUser
import io.reactivex.Observable
import io.reactivex.Observer


import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.*


interface SignUpApi {


    @GET("auth/{phoneNumber}")
    fun activateRegistration(@Path("phoneNumber") phone:String): Observable<String>


    @POST("activate/")
    fun activateSignUp(@Body user:SignUpRequest): Observable<SignUpResponse>


    @POST("signup/")
    fun requestSignUp(@Body user:SignUpRequest): Observable<SignUpResponse>

    //what I remember here the request is normally an http request
    //but the controller gets the request and converts it into an object request the use case understands


}