package com.example.usecases.signup

import android.content.Context
import androidx.annotation.NonNull
import com.example.usecases.APIClient
import com.example.usecases.utilities.PreferenceHandler
import dagger.Module
import dagger.Provides

@Module
class ModuleSignUp {


    @Provides
    fun provideSignUpUseCase(@NonNull signUpRepo: SignUpRepository): SignUp {
        return SignUp(signUpRepo)
    }

    @Provides
    fun provideSignUpRepo(@NonNull signUpApi: SignUpApi, service: RegisterSmackServiceClient, pref: PreferenceHandler): SignUpRepository {

        return SignUpRepository(signUpApi,service,pref)

    }

    @Provides
    fun provideSignUpApi(): SignUpApi {

        return APIClient().getRetrofitInstance()!!.create(SignUpApi::class.java)

    }


    fun provideSignUpService(context: Context): RegisterSmackServiceClient{

        return RegisterSmackServiceClient(context)

    }


}