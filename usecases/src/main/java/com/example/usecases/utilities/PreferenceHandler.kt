package com.example.usecases.utilities

import android.content.Context
import android.preference.PreferenceManager
import com.example.usecases.entities.ChatUser

class PreferenceHandler(val mContext: Context) {



    companion object {
        val USER_PASSWORD = "user_password"
        val USER_TOKEN = "user_token"
        val USER_EMAIL = "user_email"
    }


    fun saveUserToken(token:String){
        putPref(USER_TOKEN,token)
    }
    fun getUserToken():String{
      return  getPref(USER_TOKEN)!!
    }


    fun putChatUserPref(key: String?, value: ChatUser) {

        val name_key = key+"1"
        val pass_key = key+"2"
        val email_key = key+"3"

        putPref(name_key,value.userName)
        putPref(pass_key,value.phoneNumber)
        putPref(email_key,value.email)

    }


    fun getChatUserPref(key: String?): ChatUser? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(mContext)

        val name_key = key+"1"
        val pass_key = key+"2"
        val email_key = key+"3"

        val name =  preferences.getString(name_key, null)
        val pass =  preferences.getString(pass_key, null)
        val email =  preferences.getString(email_key, null)

        return ChatUser(name,pass,"",email,null)

    }



    fun putPref(key: String?, value: String?) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(mContext)
        val editor = prefs.edit()
        editor.putString(key, value)
        editor.apply()
    }


    fun getPref(key: String?): String? {
        val preferences =
            PreferenceManager.getDefaultSharedPreferences(mContext)
        return preferences.getString(key, null)
    }


}