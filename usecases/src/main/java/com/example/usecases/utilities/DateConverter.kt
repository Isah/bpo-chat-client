package com.example.usecases.utilities

import androidx.room.TypeConverter
import androidx.room.TypeConverters
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


@TypeConverters
class DateConverter {


    companion object{

        @TypeConverter
        fun toDate(timestamp: Long): Date {

            return  dateWithTimeToDateWithoutTime(Date(timestamp))!!
        }

        @TypeConverter
        fun  toTimestamp(date:Date):Long {
            return date.time
        }


        fun stringToDateWithoutTime(date: String?): Date? {
            var date1: Date? = null
            val dateFormatter =
                SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH)
            dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
            //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            try {
                date1 = dateFormatter.parse(date)
            } catch (e: ParseException) {
            }
            return date1
        }

        fun dateToStringWithoutTime(date: Date?): String? {
            val justDateNoTime: String
            val dateFormatter =
                SimpleDateFormat("MMMM d, yyyy", Locale.US)
            dateFormatter.timeZone = TimeZone.getTimeZone("UTC")
            //SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            justDateNoTime = dateFormatter.format(date)
            return justDateNoTime
        }


        fun dateWithTimeToDateWithoutTime(date: Date?): Date? {
            return stringToDateWithoutTime(
               dateToStringWithoutTime(date)
            )
        }

    }



}