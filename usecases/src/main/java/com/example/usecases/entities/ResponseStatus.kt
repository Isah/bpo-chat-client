package com.example.usecases.entities

data class ResponseStatus(val isSuccess: Boolean,val status_code: Int, val status_msg: String)