package com.example.usecases.entities

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "bpochat", indices = [Index(value = ["userID"], unique = true)])
class BpoChat(
    @PrimaryKey val messageId: String = UUID.randomUUID().toString(),
    var userID:String?,
    var phone:String?,
    var message: String?,
    var operationStatus: Int = 0, //not sent
    var deliveryStatus: Int = 1,  //to
    var timeStamp: Date?,
    var from: String?,
    var to: String?)




