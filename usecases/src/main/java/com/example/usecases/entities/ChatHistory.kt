package com.example.usecases.entities

data class ChatHistory (val profile: Int, val receiverName:String, val senderChat:String)