package com.example.usecases.entities

data class ChatUser(val userName:String?){

    var password: String? = null
    var phoneNumber:String? = null
    var email:String? = null


    var status: ChatUserStatus? = null


}