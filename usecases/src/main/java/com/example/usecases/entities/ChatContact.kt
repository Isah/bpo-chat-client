package com.example.usecases.entities

data class ChatContact(val username:String, val email:String, val status:String, val profile: Int)