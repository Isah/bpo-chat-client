package com.example.usecases

import com.example.usecases.entities.ChatUser
import org.hamcrest.CoreMatchers.`is`
import com.example.usecases.signup.SignUp
import com.example.usecases.signup.SignUpRepository
import com.nhaarman.mockitokotlin2.argumentCaptor
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */

class ExampleUnitTest {


    @Mock
    lateinit var signUpRepo: SignUpRepository

    @Mock
    lateinit var outputBoundary: UseCase.OutputBoundary<SignUp.ResponseValue>
    @Captor
    private val mSignUpCallbackCaptor: ArgumentCaptor<UseCase.OutputBoundary<UseCase.ResponseValue>>? = null


    //Capture values
    val phoneNoArgumentCaptor = argumentCaptor<String>()
    val tokenArgumentCaptor = argumentCaptor<String>()
    val userNameArgumentCaptor = argumentCaptor<String>()





    lateinit var signUp: SignUp


    //Dummy Data
    lateinit var dummyUser:ChatUser


    @Before
    fun setup() {

        MockitoAnnotations.initMocks(this)
        signUp = SignUp(signUpRepo)

        //Dummy Data
        dummyUser = ChatUser("Petr","758591842","petr@bposeats.com")


    }

    //TEST CASES
    /**
     * Equivalence class partitions
     * Testing for Invalid Inputs ( 3 equivalence classes )
     * 1. valid - x = size 9
      * 2. invalid - > 9,0,-ve numbers,non numbers,empty
     */

    /**
    TC1 - verify signup with valid data
    Test Scenario -  signup successfully -> basic flow
    Preconditions -  both parties online
    Test Steps    - 1. enter phone no,  2. click send button
    TestData      - ("758591842")
    Expected      - signup process starts
     */


    @Test
    fun shouldRequestTokenWhenDataIsValid(){

        val req : SignUp.RequestValue = SignUp.RequestValue(dummyUser.userName!!,dummyUser.phoneNumber!!)

        //when signup is called
        signUp.execute(req,outputBoundary)

        //2. verify the right mtd executed and captured the arguments
        Mockito.verify(signUpRepo).requestToken(phoneNoArgumentCaptor.capture())

        assertThat(phoneNoArgumentCaptor.firstValue, `is`(dummyUser.phoneNumber))

    }


    @Test
    fun shouldSignUpWhenDataIsValid(){

          val req : SignUp.RequestValue = SignUp.RequestValue(dummyUser.userName!!,dummyUser.phoneNumber!!)

          //when signup is called
          signUp.execute(req,outputBoundary)

        //2. verify the right mtd executed and captured the arguments
        Mockito.verify(signUpRepo).signUpForMessaging(userNameArgumentCaptor.capture(),phoneNoArgumentCaptor.capture(),tokenArgumentCaptor.capture())


        //Make sure the request data is captured before sending operation
        assertThat(userNameArgumentCaptor.firstValue, `is`(dummyUser.userName))

        // When msg is finally sent
        // trigger the  callback  //to simulate sending
        // means
        val response:SignUp.ResponseValue = SignUp.ResponseValue(dummyUser)
        mSignUpCallbackCaptor!!.value.onSuccess(response)


        //Check the Response
        //make sure the response value of the usecase that contains the dummy data is the one received by the outputboundary
        assertThat(dummyUser.email, `is`(SignUp.responseValue.chatUser.email))

    }





    /*

    @Test
    fun signUp_success() {
        val customer = Customer().apply {
            name = "Test Name"
            email = "test@example.com"
            phone = "0123444456789"
            phoneDdi = "+92"
            phoneNumber = ""
            countryCode = "92"
            password = "123456"
        }
        mPresenter.signUp(customer)
        verify(mView).showProgress()
        verify(mInteractor).createAccount(any(), isNull(), mArgumentCaptor.capture())
    }

     */





}
