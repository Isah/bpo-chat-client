package com.example.controllers

import com.example.usecases.signup.SignUp

interface SignUpPresenter {


    fun presentVerification(verifyMsg:String)

    fun showProgress(isProgress: Boolean)
    fun presentInValidInput(msg:String)
    fun presentError(msg:String)

    fun presentSignUpWelcome(response: SignUp.ResponseValue)



}