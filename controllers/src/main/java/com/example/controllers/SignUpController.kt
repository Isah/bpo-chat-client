package com.example.controllers

import android.app.Activity
import android.util.Log
import com.example.usecases.UseCase
import com.example.usecases.signup.SignUp
import javax.inject.Inject


class SignUpController @Inject constructor(val context: Activity, val signupPresenter: SignUpPresenter, val signup: SignUp) {




    fun createEjAccount(username:String, password: String){

        val req : SignUp.RequestValue = SignUp.RequestValue(username,"",password,"",SignUp.ACCOUNT)

        signup.execute(req,object: UseCase.OutputBoundary<SignUp.ResponseValue>{

            override fun onSuccess(result: SignUp.ResponseValue) {

                //do nothing really coz we be waiting for activation code
                //Present a UI to the user to input the activation code

                val verification = "Please Enter verification code sent to !${result.signUpResponse?.phoneNumber}"
                signupPresenter.presentVerification(verification)
                signupPresenter.showProgress(false)
            }

            override fun onNetworkFail(e: String) {

            }

            override fun onInvalidMessage(e: String) {

                signupPresenter.presentInValidInput(e)

            }

        })



    }


    fun register(username:String, password: String, phone:String){
        signupPresenter.showProgress(true)

        val req : SignUp.RequestValue = SignUp.RequestValue(username,phone,password,"",SignUp.RIGISTER)
                           //This is the client object that impl the use case interface
        signup.execute(req,object: UseCase.OutputBoundary<SignUp.ResponseValue>{

            override fun onSuccess(result: SignUp.ResponseValue) {

                //do nothing really coz we be waiting for activation code
                //Present a UI to the user to input the activation code

                val verification = "Please Enter verification code sent to !${result.signUpResponse?.phoneNumber} "
                signupPresenter.presentVerification(verification)
                signupPresenter.showProgress(false)
            }

            override fun onNetworkFail(e: String) {

            }

            override fun onInvalidMessage(e: String) {
                signupPresenter.showProgress(false)
                signupPresenter.presentInValidInput(e)

            }

        })

    }


    fun activateUser(username: String, password: String, phone:String,activation_code:String){

        signupPresenter.showProgress(true)

        val req : SignUp.RequestValue = SignUp.RequestValue(username,phone,password,activation_code,SignUp.ACTIVATE)

        signup.execute(req, object: UseCase.OutputBoundary<SignUp.ResponseValue>{

            override fun onSuccess(result: SignUp.ResponseValue) {

              signupPresenter.showProgress(false)
              signupPresenter.presentSignUpWelcome(result)

              // registrationService(result.chatUser?.userName,result.chatUser?.password)

            }

            override fun onInvalidMessage(e:String) {


           
            }

            override fun onNetworkFail(e:String) {
                Log.d("RegMsg", e)

            }

        })


    }








}