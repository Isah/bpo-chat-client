package com.example.controllers

import android.app.Activity
import android.content.Context
import androidx.annotation.NonNull
import com.example.usecases.chat.GetChats
import com.example.usecases.chat.SendChat
import com.example.usecases.signup.SignUp
import dagger.Module
import dagger.Provides

@Module
class ModuleControllers {

    @Provides
    fun provideSignUpController(context: Activity,presenter: SignUpPresenter, signup:SignUp): SignUpController {
        return SignUpController(context,presenter,signup)
    }

    @Provides
    fun provideChatsController(presenter: ChatsPresenter, sendChat: SendChat, getChats: GetChats): ChatsController {
        return ChatsController(presenter,sendChat,getChats)
    }



}