package com.example.controllers

import com.example.usecases.chat.GetChats

interface ChatsPresenter {

    fun presentChats(result: GetChats.ResponseValue)
    fun presentNoChats()

}