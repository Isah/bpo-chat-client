package com.example.controllers

import com.example.usecases.UseCase
import com.example.usecases.chat.GetChats
import com.example.usecases.chat.SendChat
import com.example.usecases.entities.BpoChat
import com.example.usecases.entities.ChatUser
import com.example.usecases.signup.SignUp

class ChatsController(val presenter: ChatsPresenter, val sendChat: SendChat,val viewChats: GetChats ) {


       fun executeChat(chatUser: ChatUser, chat:BpoChat){

           val req = SendChat.RequestValue(chatUser,chat)

           sendChat.execute(req, object : UseCase.OutputBoundary<SendChat.ResponseValue> {

               override fun onSuccess(result: SendChat.ResponseValue) {

                    //if sending is success full call get chats to populate the UI
                     read_chats(chatUser)

               }

               override fun onNetworkFail(e: String) {

               }

               override fun onInvalidMessage(e: String) {

               }


           })




       }




    fun executeReadIncomingChats(chatUser: ChatUser){

        val request = GetChats.RequestValue(GetChats.CHAT,chatUser)


        viewChats.execute(request, object: UseCase.OutputBoundary<GetChats.ResponseValue>{

            override fun onSuccess(result: GetChats.ResponseValue) {

                 read_chats(chatUser)

            }


            override fun onNetworkFail(e: String) {

            }

            override fun onInvalidMessage(e: String) {

            }


        })



    }


    private fun read_chats(chatUser: ChatUser){

        val request = GetChats.RequestValue(GetChats.LIST_CHATS,chatUser)

        viewChats.execute(request, object: UseCase.OutputBoundary<GetChats.ResponseValue>{

            override fun onSuccess(result: GetChats.ResponseValue) {

                presenter.presentChats(result)

            }


            override fun onNetworkFail(e: String) {

            }

            override fun onInvalidMessage(e: String) {

            }


        })


    }




}